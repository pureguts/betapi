class CreateBets < ActiveRecord::Migration[5.1]
  def change
    create_table :bets do |t|
      t.references :user, foreign_key: true
      t.string :direct_upload_url, null: false
      t.attachment :media
      t.integer :status, default: 0, null: false
      t.timestamps null: false
    end
    #add_index :bets, :user_id
    add_index :bets, :status
  end
end
