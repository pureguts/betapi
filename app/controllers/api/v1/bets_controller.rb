class Api::V1::BetsController < ApplicationController

  before_action :authenticate_with_token!, only: [:create]
  
  def index
    #@bets = Bet.all
    #json_response(@bets)
    bets = Bet.all
    #json_response(@bets)
    render json:  { 
      bets: bets.map { |r| r.attributes.merge(upload: r.media, direct_upload_url: r.direct_upload_url, creator: r.user.image, thumb: r.media.url(:thumb) )}, 
      is_success: true
      }, status: :ok
  end

  #def new
  #  @bet = current_user.bets.build(bet_params)
  #end

  

  def create

    

    @create_bet_service = CreateBet.new(bet_params)

    @bet = @create_bet_service.bet
    @bet.user = current_user

     
    if @create_bet_service.call
      render json: { is_success: true}, status: :ok
    else
      render json: { errors: @bet.errors.messages }, status: :unprocessable_entity
    end
  end

  private

  def set_bet
    @bet = Bet.find(params[:id])
  end 

  def bet_params
    params.require(:bet).permit(:direct_upload_url)
  end
end