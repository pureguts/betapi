module Response
  def json_response(object)
    render json: object, is_success: true, status: :ok
  end
end