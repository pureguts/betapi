class UserSerializer < ActiveModel::Serializer
  attributes :email, :fullname, :image, :access_token
end
