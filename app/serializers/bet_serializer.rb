class BetSerializer < ActiveModel::Serializer

  attributes :id, :direct_upload_url, :media

  def direct_upload_url
    object.direct_upload_url
  end

  class UserSerializer < ActiveModel::Serializer
    attributes  :fullname, :image
  end

  belongs_to :user, serializer: UserSerializer, key: :creator
end
