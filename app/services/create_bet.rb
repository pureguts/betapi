class CreateBet

attr_reader  :params, :bet, :current_user



def initialize(params)
    @params = params

    build
end

def call

	saved = @bet.save
	if saved
	  queue_process
	end
	saved
end

private

  def build
    @bet = Bet.new(@params) 
  end

  def queue_process
    ProcessBetJob.perform_later(@bet)
  end

end