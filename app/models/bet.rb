class Bet < ApplicationRecord

   ##DIRECT_UPLOAD_URL_FORMAT = %r{
    #\A
    #https:\/\/
    ##{Rails.application.secrets.aws['s3_bucket_name']}\.s3\.amazonaws\.com\/
    #(?<path>uploads\/.+\/(?<filename>.+))
    #\z
  ##}x.freeze

  belongs_to :user

  

  enum status: { unprocessed: 0, processed: 1 }

  has_attached_file :media,
  :styles => {:thumb => ["400x400#", :jpg]}, :processors => [:transcoder],
  :storage => :s3,
  :bucket  => 'bakkar-video',
  :url => ':s3_alias_url',
  :s3_host_alias => "d364is0k9t9xh3.cloudfront.net",
  :path => '/:class/:attachment/:id_partition/:style/:filename'
  #:url =>':s3_domain_url.to_s',
  #:path => ":rails_root/files/:assetable_id/:basename.:extension"
  

  validates :direct_upload_url, presence: true
  do_not_validate_attachment_file_type :media

end



