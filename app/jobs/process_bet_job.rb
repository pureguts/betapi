class ProcessBetJob < ActiveJob::Base
  queue_as :default

  def perform(bet)
    bet.media = URI.parse(bet.direct_upload_url)
    bet.status = "processed"
    bet.save!
  end

end